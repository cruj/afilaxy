package br.com.cruj.afilaxy.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.ui.common.ChangeEventType;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import br.com.cruj.afilaxy.R;
import br.com.cruj.afilaxy.extras.ChatHolder;
import br.com.cruj.afilaxy.model.ChatMessage;


/**
 * Created by cruj on 08/03/18.
 */

public class ChatFragment extends Fragment {
    private static final int SIGN_IN_REQUEST_CODE = 123;
    private Context mContext;
    private SupportMapFragment mMapFragment;
    private String pacienteId = "";
    private Button btnLogout;
    private FirebaseRecyclerAdapter<ChatMessage,ChatHolder> adapter;
    private RecyclerView mRecyclerView;
    private EditText mEdit;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        mContext = getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);
        FloatingActionButton fab =
                view.findViewById(R.id.fab);
        mEdit = view.findViewById(R.id.et_input_message);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Read the input field and push a new instance
                // of ChatMessage to the Firebase database
                FirebaseDatabase.getInstance()
                        .getReference().child("chat")
                        .push()
                        .setValue(new ChatMessage(mEdit.getText().toString(),
                                FirebaseAuth.getInstance()
                                        .getCurrentUser()
                                        .getDisplayName())
                        );

                // Clear the input
                Bundle bundle = new Bundle();
                bundle.putString("message", mEdit.getText().toString());

                mFirebaseAnalytics.logEvent("send_message", bundle);
                mEdit.setText("");
            }
        });

        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            // Start sign in/sign up activity
            Toast.makeText(mContext,
                    "ERRO ",
                    Toast.LENGTH_LONG)
                    .show();
        } else {
            // User is already signed in. Therefore, display
            // a welcome Toast
            Toast.makeText(mContext,
                    "Welcome " + FirebaseAuth.getInstance()
                            .getCurrentUser()
                            .getDisplayName(),
                    Toast.LENGTH_LONG)
                    .show();

            // Load chat room contents
            displayChatMessages(view);
        }
        //getAssignedPaciente();


        return view;
    }

    private void displayChatMessages(View v) {
        mRecyclerView = v.findViewById(R.id.list_of_messages);
        Query query = FirebaseDatabase.getInstance().getReference().child("chat").limitToLast(8);
        FirebaseRecyclerOptions<ChatMessage> options =
                new FirebaseRecyclerOptions.Builder<ChatMessage>()
                        .setQuery(query, ChatMessage.class)
                        .build();
        adapter = new FirebaseRecyclerAdapter<ChatMessage, ChatHolder>(options) {
            @Override
            public void onChildChanged(@NonNull ChangeEventType type, @NonNull DataSnapshot snapshot, int newIndex, int oldIndex) {
                super.onChildChanged(type, snapshot, newIndex, oldIndex);
                mRecyclerView.scrollToPosition(newIndex);
            }

            @Override
            public ChatHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message, parent, false);
                return new ChatHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull ChatHolder holder, int position, @NonNull ChatMessage model) {
                holder.messageText.setText(model.getMessageText());
                holder.messageUser.setText(model.getMessageUser());

                // Format the date before showing it
                holder.messageTime.setText(DateFormat.format("dd-MM-yyyy (HH:mm:ss)",
                        model.getMessageTime()));
            }
        };


        /*
        adapter = new FirebaseRecyclerAdapter<ChatMessage>(options) {
            @Override
            protected void populateView(View v, ChatMessage model, int position) {
                // Get references to the views of message.xml
                TextView messageText = (TextView) v.findViewById(R.id.message_text);
                TextView messageUser = (TextView) v.findViewById(R.id.message_user);
                TextView messageTime = (TextView) v.findViewById(R.id.message_time);

                // Set their text
                messageText.setText(model.getMessageText());
                messageUser.setText(model.getMessageUser());

                // Format the date before showing it
                messageTime.setText(DateFormat.format("dd-MM-yyyy (HH:mm:ss)",
                        model.getMessageTime()));
            }
        };
*/
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setReverseLayout(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);

        mRecyclerView.setAdapter(adapter);
        adapter.startListening();
    }

    private void getAssignedPaciente() {
        String anjoId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference assignedPacienteRef = FirebaseDatabase.getInstance().getReference().child("Users").child("Anjo").child(anjoId).child("pacienteAjudaId");

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void removeReferences() {
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference anjosAvaiableRef = FirebaseDatabase.getInstance().getReference("anjosAvaiable");
        DatabaseReference anjosWorkingRef = FirebaseDatabase.getInstance().getReference("anjosWorking");
    }
}
