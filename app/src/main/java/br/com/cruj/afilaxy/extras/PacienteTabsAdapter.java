package br.com.cruj.afilaxy.extras;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import br.com.cruj.afilaxy.R;
import br.com.cruj.afilaxy.fragments.ChatFragment;
import br.com.cruj.afilaxy.fragments.ConfigFragment;
import br.com.cruj.afilaxy.fragments.PacienteMapaFragment;

/**
 * Created by cruj on 08/03/18.
 */

public class PacienteTabsAdapter extends FragmentPagerAdapter {
    private Context mContext;
    private String[] titles = {"MAPA", "CHAT", "CONFIG"};
    private int[] icons = new int[]{R.drawable.ic_inhalator, R.drawable.ic_inhalator, R.drawable.ic_inhalator};
    private int heightIcon;


    public PacienteTabsAdapter(FragmentManager fm, Context c) {
        super(fm);

        mContext = c;
        double scale = c.getResources().getDisplayMetrics().density;
        heightIcon = (int)( 24 * scale + 0.5f );
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag = null;

        if(position == 0){ // ALL CARS
            frag = new PacienteMapaFragment();
        }
        else if(position == 1){ // LUXURY CAR
            frag = new ChatFragment();
        }
        else if(position == 2){ // SPORT CAR
            frag = new ConfigFragment();
        }

        Bundle b = new Bundle();
        b.putInt("position", position);

        frag.setArguments(b);

        return frag;
    }

    @Override
    public int getCount() {
        return titles.length;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        /*Drawable d = mContext.getResources().getDrawable( icons[position] );
        d.setBounds(0, 0, heightIcon, heightIcon);

        ImageSpan is = new ImageSpan( d );

        SpannableString sp = new SpannableString(" ");
        sp.setSpan( is, 0, sp.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );


        return ( sp );*/
        Log.d("debug_tabs","tab - " + titles[position]);
        return ( titles[position] );
    }
}