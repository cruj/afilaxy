package br.com.cruj.afilaxy.fragments;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import br.com.cruj.afilaxy.R;

/**
 * Created by cruj on 08/03/18.
 */

public class AnjoMapaFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
    private GoogleMap mMap;
    private Context mContext;
    GoogleApiClient mGoogleAPiClient;
    Location mLastLocation;
    LocationRequest mLocationRequest;
    private SupportMapFragment mMapFragment;
    private String pacienteId = "";
    private Button btnLogout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_anjo_map, container, false);
        mContext = getActivity();
        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);
        /*SupportMapFragment mapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);*/

        btnLogout = view.findViewById(R.id.btn_logout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeReferences();
                FirebaseAuth.getInstance().signOut();
                /*Intent i = new Intent(mContext, MainActivity.class);
                startActivity(i);
                finish();*/
                return;
            }
        });
        getAssignedPaciente();


        return view;
    }

    private void getAssignedPaciente() {
        String anjoId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference assignedPacienteRef = FirebaseDatabase.getInstance().getReference().child("Users").child("Anjo").child(anjoId).child("pacienteAjudaId");
        assignedPacienteRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    pacienteId = dataSnapshot.getValue().toString();
                    getAssignedPickupLocation();
                } else {
                    pacienteId = "";
                    if (pacienteMarker != null)
                        pacienteMarker.remove();
                    if (assignedPacientePickupLocationRefListener != null)
                        assignedPacientePickupLocationRef.removeEventListener(assignedPacientePickupLocationRefListener);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    Marker pacienteMarker;
    private DatabaseReference assignedPacientePickupLocationRef;
    private ValueEventListener assignedPacientePickupLocationRefListener;

    private void getAssignedPickupLocation() {
        assignedPacientePickupLocationRef = FirebaseDatabase.getInstance().getReference().child("pacienteRequest").child(pacienteId).child("l");
        assignedPacientePickupLocationRefListener = assignedPacientePickupLocationRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && !pacienteId.equals("")) {
                    List<Object> map = (List<Object>) dataSnapshot.getValue();
                    double locationLat = 0;
                    double locationLng = 0;
                    if (map.get(0) != null) {
                        locationLat = Double.parseDouble(map.get(0).toString());
                    }
                    if (map.get(1) != null) {
                        locationLng = Double.parseDouble(map.get(1).toString());
                    }
                    LatLng anjoLatLng = new LatLng(locationLat, locationLng);
                    pacienteMarker = mMap.addMarker(new MarkerOptions().position(anjoLatLng).title("Paciente está aqui"));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        buildGoogleApiCLient();
        mMap.setMyLocationEnabled(true);
    }

    protected synchronized void buildGoogleApiCLient() {
        mGoogleAPiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleAPiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (mContext.getApplicationContext() != null) {

            mLastLocation = location;
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(13));

            String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
            DatabaseReference refAvaiable = FirebaseDatabase.getInstance().getReference("anjosAvaiable");
            DatabaseReference refWorking = FirebaseDatabase.getInstance().getReference("anjosWorking");
            GeoFire geoFireAvaiable = new GeoFire(refAvaiable);
            GeoFire geoFireWorking = new GeoFire(refWorking);


            switch (pacienteId) {
                case "":
                    geoFireWorking.removeLocation(userId, mGeofireCompletionListener);
                    geoFireAvaiable.setLocation(userId, new GeoLocation(location.getLatitude(), location.getLongitude()), mGeofireCompletionListener);
                    break;
                default:
                    geoFireAvaiable.removeLocation(userId, mGeofireCompletionListener);
                    geoFireWorking.setLocation(userId, new GeoLocation(location.getLatitude(), location.getLongitude()), mGeofireCompletionListener);
                    break;
            }

        }
    }

    private GeoFire.CompletionListener mGeofireCompletionListener = new GeoFire.CompletionListener() {
        @Override
        public void onComplete(String key, DatabaseError error) {

        }
    };

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleAPiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void removeReferences() {
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference anjosAvaiableRef = FirebaseDatabase.getInstance().getReference("anjosAvaiable");
        DatabaseReference anjosWorkingRef = FirebaseDatabase.getInstance().getReference("anjosWorking");

        GeoFire geoFireAvaiable = new GeoFire(anjosAvaiableRef);
        geoFireAvaiable.removeLocation(userId, mGeofireCompletionListener);
        GeoFire geoFireWorking = new GeoFire(anjosWorkingRef);
        geoFireWorking.removeLocation(userId, mGeofireCompletionListener);
    }


}
