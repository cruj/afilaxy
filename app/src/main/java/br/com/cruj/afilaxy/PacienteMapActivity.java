package br.com.cruj.afilaxy;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.List;

public class PacienteMapActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private GoogleMap mMap;

    GoogleApiClient mGoogleAPiClient;
    Location mLastLocation;
    LocationRequest mLocationRequest;
    private LatLng pacienteLocation;
    private Button btnLogout, btnChamarAnjo;
    private Boolean requestBol = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paciente_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        btnLogout = findViewById(R.id.btn_logout);
        btnChamarAnjo = findViewById(R.id.btn_chamar_anjo);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                Intent i = new Intent(PacienteMapActivity.this, MainActivity.class);
                startActivity(i);
                finish();
                return;
            }
        });

        btnChamarAnjo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (requestBol) {
                    requestBol = false;
                    geoQuery.removeAllListeners();
                    anjoLocationRef.removeEventListener(anjoLocationEventListener);
                    if (anjoFoundId != null) {
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Users").child("Anjo").child(anjoFoundId);
                        ref.setValue(true);
                        anjoFoundId = null;
                    }
                    anjoFound = false;
                    radius = 1;
                    String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("pacienteRequest");
                    GeoFire geoFire = new GeoFire(ref);
                    geoFire.removeLocation(userId, mGeofireCompletionListener);
                    if (mAnjoMarker != null)
                        mAnjoMarker.remove();

                    if (mPacienteMarker != null)
                        mPacienteMarker.remove();

                    btnChamarAnjo.setText("Chamar Anjo");
                } else {
                    requestBol = true;
                    String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("pacienteRequest");
                    GeoFire geoFire = new GeoFire(ref);
                    geoFire.setLocation(userId, new GeoLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude()), mGeofireCompletionListener);
                    pacienteLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                    mPacienteMarker = mMap.addMarker(new MarkerOptions().position(pacienteLocation).title("Me Ajude Aqui").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_user)));
                    btnChamarAnjo.setText("Achando seu anjo");
                    getClosestAnjo();
                }
            }
        });
    }

    private int radius = 1;
    private Boolean anjoFound = false;
    private String anjoFoundId;
    GeoQuery geoQuery;

    private void getClosestAnjo() {
        DatabaseReference anjoLocation = FirebaseDatabase.getInstance().getReference().child("anjosAvaiable");
        GeoFire geoFire = new GeoFire(anjoLocation);
        geoQuery = geoFire.queryAtLocation(new GeoLocation(pacienteLocation.latitude, pacienteLocation.longitude), radius);
        geoQuery.removeAllListeners();
        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                if (!anjoFound && requestBol) {
                    anjoFound = true;
                    anjoFoundId = key;

                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Users").child("Anjo").child(anjoFoundId);
                    String pacienteId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    HashMap map = new HashMap();
                    map.put("pacienteAjudaId", pacienteId);
                    ref.updateChildren(map);

                    getAnjoLocation();
                    btnChamarAnjo.setText("Buscando Localização");

                }
            }

            @Override
            public void onKeyExited(String key) {

            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {

            }

            @Override
            public void onGeoQueryReady() {
                if (!anjoFound) {
                    radius++;
                    getClosestAnjo();
                }
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {

            }
        });
    }

    private Marker mAnjoMarker;
    private Marker mPacienteMarker;
    private DatabaseReference anjoLocationRef;
    private ValueEventListener anjoLocationEventListener;

    private void getAnjoLocation() {
        anjoLocationRef = FirebaseDatabase.getInstance().getReference().child("anjosWorking").child(anjoFoundId).child("l");
        anjoLocationEventListener = anjoLocationRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && requestBol) {
                    List<Object> map = (List<Object>) dataSnapshot.getValue();
                    double locationLat = 0;
                    double locationLng = 0;
                    btnChamarAnjo.setText("Anjo Encontrado");
                    if (map.get(0) != null) {
                        locationLat = Double.parseDouble(map.get(0).toString());
                    }
                    if (map.get(1) != null) {
                        locationLng = Double.parseDouble(map.get(1).toString());
                    }
                    LatLng anjoLatLng = new LatLng(locationLat, locationLng);
                    if (mAnjoMarker != null) {
                        mAnjoMarker.remove();
                    }
                    Location loc1 = new Location("");
                    loc1.setLatitude(pacienteLocation.latitude);
                    loc1.setLongitude(pacienteLocation.longitude);

                    Location loc2 = new Location("");
                    loc2.setLatitude(anjoLatLng.latitude);
                    loc2.setLongitude(anjoLatLng.longitude);
                    float distance = loc1.distanceTo(loc2);
                    if (distance < 100) {
                        btnChamarAnjo.setText("O anjo chegou");

                    } else {
                        btnChamarAnjo.setText("Distância: " + String.format("%.0f", distance) + " metros");
                    }
                    mAnjoMarker = mMap.addMarker(new MarkerOptions().position(anjoLatLng).title("Seu anjo").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_inhalator)));

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        buildGoogleApiCLient();
        mMap.setMyLocationEnabled(true);
    }

    protected synchronized void buildGoogleApiCLient() {
        mGoogleAPiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleAPiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleAPiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private GeoFire.CompletionListener mGeofireCompletionListener = new GeoFire.CompletionListener() {
        @Override
        public void onComplete(String key, DatabaseError error) {

        }
    };
}
