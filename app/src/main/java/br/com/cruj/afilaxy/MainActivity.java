package br.com.cruj.afilaxy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnAnjo,btnPaciente;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnAnjo = findViewById(R.id.btn_anjo);
        btnPaciente = findViewById(R.id.btn_paciente);

        btnAnjo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,AnjoLoginActivity.class);
                startActivity(i);
                finish();
                return;
            }
        });
        btnPaciente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,PacienteLoginActivity.class);
                startActivity(i);
                finish();
                return;
            }
        });
    }
}
