package br.com.cruj.afilaxy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseListAdapter;

import br.com.cruj.afilaxy.extras.PacienteTabsAdapter;
import br.com.cruj.afilaxy.extras.SlidingTabLayout;
import br.com.cruj.afilaxy.model.ChatMessage;

/**
 * Created by cruj on 08/03/18.
 */

public class PacienteActivity extends AppCompatActivity {
    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;
    private Toolbar mToolbar;
    private TabLayout mTabLayout;
    private TextView titleToolbar;
    private FirebaseListAdapter<ChatMessage> adapter;
    private void setupToolbar(final Bundle savedInstanceState) {
        mToolbar = findViewById(R.id.tb_anjo_main);
        this.setSupportActionBar(mToolbar);
        if (this.getSupportActionBar() != null) {
            this.getSupportActionBar().setTitle("");
        }

    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anjo_main);
        titleToolbar = (TextView) findViewById(R.id.toolbar_title);

        titleToolbar.setText("Afilaxy");
        mViewPager = findViewById(R.id.vp_tabs);
        mTabLayout = findViewById(R.id.stl_tabs);
        mTabLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mViewPager.setAdapter( new PacienteTabsAdapter( getSupportFragmentManager(), this ));
        mTabLayout.setupWithViewPager(mViewPager);

        /*mSlidingTabLayout = findViewById(R.id.stl_tabs);
        //mSlidingTabLayout.setDistributeEvenly(true);
        mSlidingTabLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.colorFAB));
        mSlidingTabLayout.setCustomTabView(R.layout.tab_view, R.id.tv_tab);
        mSlidingTabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
            @Override
            public void onPageSelected(int position) {
            }
            @Override
            public void onPageScrollStateChanged(int state) {}
        });
        mSlidingTabLayout.setViewPager( mViewPager );*/
        setupToolbar(savedInstanceState);

    }
}
