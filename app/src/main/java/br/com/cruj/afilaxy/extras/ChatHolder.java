package br.com.cruj.afilaxy.extras;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import br.com.cruj.afilaxy.R;

/**
 * Created by cruj on 08/03/18.
 */

public class ChatHolder extends RecyclerView.ViewHolder {
    public TextView messageText;
    public TextView messageUser;
    public TextView messageTime;

    public ChatHolder(View v) {
        super(v);
        messageText = (TextView) v.findViewById(R.id.message_text);
        messageUser = (TextView) v.findViewById(R.id.message_user);
        messageTime = (TextView) v.findViewById(R.id.message_time);

    }
}